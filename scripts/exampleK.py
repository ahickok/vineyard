import matplotlib.pyplot as plt
import math
import numpy as np

def plot(polys, triangulate):
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink']
    plt.figure(figsize = (5, 5))
    plt.axis('off')
    for i, P in enumerate(polys):
        plt.fill(P[:, 0], P[:, 1], facecolor = colors[i], edgecolor = 'k')
        if triangulate and len(P) > 3:
            x0 = P[0, 0]
            y0 = P[0, 1]
            for p in P[1:-1, :]:
                xs = [x0, p[0]]
                ys = [y0, p[1]]
                plt.plot(xs, ys, color = 'k')
                
granada = np.array([[.7*math.cos(2*math.pi*k/5), .7*math.sin(2*math.pi*k/5)] for k in range(5)])
porter = np.array([granada[1, :], granada[2, :], granada[2, :] + [-.3, .7]])
ridge = np.array([porter[2, :], granada[3, :] + [-.2, -.5], granada[3, :], granada[2, :]])
hills = np.array([ridge[2, :], ridge[1, :], ridge[1, :] + [1.5, -.5], granada[4, :]])
mission = np.array([granada[0, :], granada[4, :], hills[2, :]])
polys = [granada, mission, porter, ridge, hills]
plot(polys, True)
plt.show()
plt.savefig('../figures/LA/exampleK.png', bbox_inches = 'tight')

ktown = np.array([[math.cos(2*math.pi*k/7 - math.pi/2), math.sin(2*math.pi*k/7 - math.pi/2)] for k in range(7)])
bang = np.array([ktown[3, :], ktown[4, :], [0, 1.2]])
w = 3.5
h = 2.5
square = [ktown[0] + [-w/2, 0], ktown[0] + [w/2, 0], ktown[0] + [w/2, h], ktown[0] + [-w/2, h]]
country = np.array([ktown[6, :], ktown[0, :], square[0]])
pico = np.array([square[1], square[2], ktown[2, :], ktown[1, :]])
harvard = np.array([square[1], ktown[1, :], ktown[0, :]])
han = np.array([square[0], square[3], ktown[-2, :], ktown[-1, :]])
wilshire = np.array([square[2], square[3], ktown[-2, :], ktown[-3, :], bang[2, :], ktown[3, :], ktown[2, :]])
polys = [ktown, bang, country, pico, harvard, han, wilshire]
plot(polys, False)
for i, P in enumerate(polys[:-1]):
    if len(P) > 3:
        x0 = P[0, 0]
        y0 = P[0, 1]
        for p in P[1:-1, :]:
            xs = [x0, p[0]]
            ys = [y0, p[1]]
            plt.plot(xs, ys, color = 'k')
x0 = wilshire[0, 0]
y0 = wilshire[0, 1]
for p in wilshire[4:6, :]:
    xs = [x0, p[0]]
    ys = [y0, p[1]]
    plt.plot(xs, ys, color = 'k')
# The two curved edges in Wilshire need to be added manually
plt.savefig('../figures/LA/ktown_almostscom.png', bbox_inches = 'tight')