import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
from matplotlib.colors import LinearSegmentedColormap
    
def make_cmap(percent, sublevel = True):
    ncolors = 500
    color_array = plt.get_cmap('viridis')(range(ncolors))
    if sublevel:
        color_array[:, -1] = [1 if color < percent*ncolors else 0 for color in range(ncolors)]
    else:
        color_array = np.flip(color_array, 0)
        color_array[:, -1] = [1 if color > (1- percent)*ncolors else 0 for color in range(ncolors)]
    map_object = LinearSegmentedColormap.from_list(name = 'viridis_alpha', colors = color_array)
    plt.register_cmap(cmap = map_object)
    
peak1 = [2.5, 2.5]
peak2 = [-2.5, -2.5]
lim = 5
x = np.linspace(-8.2, 8.2, 1000)
X, Y = np.meshgrid(x, x)
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X; pos[:, :, 1] = Y

# Sublevel filtration: Well-separated local maxima
var1 = 1.5
rv1 = multivariate_normal(peak1, [[var1, 0], [0, var1]])
Z1 = rv1.pdf(pos)
rv2 = multivariate_normal(peak2, [[var1, 0], [0, var1]])
Z2 = .5*rv2.pdf(pos)
Z = Z1 + Z2
percents = [1, .5, .3, .15, .01]
for idx, pct in enumerate(percents):
    fig = plt.figure(figsize = (10, 10))
    make_cmap(pct)
    ax = plt.axes(projection = '3d')
    plt.axis('off')
    ax.plot_surface(X, Y, Z, cmap = 'viridis_alpha', linewidth = 0)
    plt.savefig(f"../figures/sublevel/sublevel_sep{idx}",  bbox_inches = 'tight')
    plt.show()

# Sublevel filtration: less separated local maxima
var2 = 5/1.5
rv3 = multivariate_normal(peak1, [[var2, 0], [0, var2]])
Z3 = rv3.pdf(pos)
rv4 = multivariate_normal(peak2, [[var2, 0], [0, var2]])
Z4 = .5*rv4.pdf(pos)
Z = Z3 + Z4
#percents = [1, .75, .5, .4, .3, .15, .1, .01]
for idx, pct in enumerate(percents):
    fig = plt.figure(figsize = (10, 10))
    make_cmap(pct)
    ax = plt.axes(projection = '3d')
    plt.axis('off')
    ax.plot_surface(X, Y, Z, cmap='viridis_alpha', linewidth = 0)
    plt.savefig(f"../figures/sublevel/sublevel_nonsep{idx}", bbox_inches = 'tight')
    plt.show()
    
# Superlevel filtration
Z = -Z3 - Z4
percents = [1, .5, .3, .1]
for idx, pct in enumerate(percents):
    fig = plt.figure(figsize = (10, 10))
    make_cmap(pct, False)
    ax = plt.axes(projection = '3d')
    plt.axis('off')
    ax.plot_surface(X, Y, Z, cmap = 'viridis_alpha', linewidth = 0)
    plt.savefig(f"../figures/superlevel/superlevel_{idx}",  bbox_inches = 'tight')
    plt.show()
