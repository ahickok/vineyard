# -*- coding: utf-8 -*-
"""
Test RU reduction algorithm
"""
import vineyard.vineyard as vy

simplices = vy.read_simplices("complex_input_test.txt")
test_vy = vy.Vineyard(simplices, 1)
print("U matrix")
test_vy.curr_RU.U.print()
print("\nR matrix")
test_vy.curr_RU.R.print()
print("Original pairings for dimension 1: ", test_vy.curr_pairs())