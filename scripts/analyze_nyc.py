from vineyard.polygonal_surface import PolygonalSurface as PS
import openpyxl
import matplotlib.pyplot as plt

def read_borough_zips():
    '''
    Source: https://www.nycbynatives.com/nyc_info/new_york_city_zip_codes.php
    Accessed March 30, 2021.
    '''
    fname = '../data/borough_zips.txt'
    zip_borough = {}
    with open(fname, "r") as f:
        lines = f.readlines()
        for L in lines:
            zipcode = L.split()[0]
            borough = ' '.join(L.split()[1:])
            zip_borough.update({zipcode : borough})
    return zip_borough
    
def read_nyc_data(data_type, per_capita = False):
    '''
    Parameters
    ----------
    data_type : {'fullvax', 'partialvax'}

    Returns
    -------
    region_vals : dictionary
        Keys are zipcodes (strings). Values are the vaccination rate (fully 
        vaccinated or partially vaccinated depending on data_type). Total number
        or per capita number depending on per_capita.)
    '''
    region_vals = {}
    file = openpyxl.load_workbook('../data/NYC_zip_vaccinations.xlsx')
    sheet = file.active
    cols = {'fullvax' : 5, 'partialvax' : 6}
    data_col = cols[data_type]
    zip_col = 3
    pop_col = 9
    first_row = 3
    last_row = 179
    for i in range(first_row, last_row + 1):
        val = sheet.cell(i, data_col).value
        if per_capita:
            val /= sheet.cell(i, pop_col).value
        zipcode = str(sheet.cell(i, zip_col).value)
        region_vals.update({zipcode : val})
    return region_vals

# Modified ZCTA (this is what's used in the vaccination data)
nyc_ps = PS.read_adj('../data/nyc_modzcta_adj.txt')
nyc_ps.triangulate()

zip_names = list(nyc_ps.R.keys())
datatype = 'fullvax'
per_capita = True
    
zip_vals = read_nyc_data(datatype, per_capita = per_capita)
zip_borough = read_borough_zips()
legend_names = ['Brooklyn', 'The Bronx', 'Manhattan', 'Queens', 'Staten Island']
legend_markers = {'Brooklyn' : 'o', 'The Bronx' : "*", 'Manhattan' : "s", 'Queens' : "P", 'Staten Island' : "^"}

filtration_type = input("Filtration type? [sub/super] ")+"level"
alternative = (input("Alternative? [y/n] ") == 'y')
if filtration_type == "superlevel":
    nyc_sc = nyc_ps.superlevel_SC(zip_vals, alternative)
else:
    nyc_sc = nyc_ps.sublevel_SC(zip_vals, alternative)
nyc_sc.persistence(homology_coeff_field = 2)

if datatype == 'fullvax':
    data_name = "full vaccinations"
else:
    data_name = "partial vaccinations"
if per_capita:
    suptitle = "NYC per capita " + data_name + " by zipcode"
else:
    suptitle = "NYC " + data_name + " by zipcode"
if alternative:
    title = "1D PH alternative " + filtration_type + " filtration"
else:
    title = "1D PH " + filtration_type + " filtration"

paper_format = (input("Paper format? [y/n] ") == 'y')
if paper_format:
    title = None
    suptitle = None
nyc_ps.plot_1d_finite_PD(nyc_sc, title = title, suptitle = suptitle, 
                         rgn_to_legendname = zip_borough, 
                         legend_names = legend_names, 
                         legend_markers = legend_markers, annotate = False)
save_fig = (input("Save figure? [y/n] ") == 'y')
if save_fig:
    fname = f"../figures/NYC/nyc_{datatype}"
    if per_capita:
        fname += "_percapita"
    fname += f"_{filtration_type}"
    if alternative:
        fname += "_alternative"
    fname += ".png"
    plt.savefig(fname)
    print(fname)
    
plot_data = (input("Plot data? (requires geopandas) [y/n] ") == 'y')
if plot_data:
    import geopandas as gpd
    import vineyard.triangulate_map as tm
    def plot_nyc_data(zip_vals = None, data_type = None, per_capita = False):
        assert zip_vals is not None or data_type is not None
        if zip_vals is None:
            zip_vals = read_nyc_data(data_type, per_capita)
        nyc_shp = '../data/NYC_modzcta/geo_export_90184b1b-c30a-4cf2-a054-e1cf140844fc.shp'
        nyc_gdf = gpd.read_file(nyc_shp)
        name_col = 'modzcta'
        tm.plot_attribute(nyc_gdf, name_col, zip_vals)
    