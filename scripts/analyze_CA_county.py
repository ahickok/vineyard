# -*- coding: utf-8 -*-
"""
Analyze CA county covid data using a vineyard
"""
import sys
sys.path.append('C:\\Users\\abiga') 
from vineyard.polygonal_surface import PolygonalSurface as PS
import xlrd
import openpyxl
import pickle

def county_values_at_time(county_map, t):
    # Only for debugging
    county_names = list(county_map.R.keys())
    county_names.remove("exterior")
    county_values = read_CA_county_data(county_names)
    county_vals_time_t = {county : county_values[county][t] for county in county_names}
    
    for county in county_vals_time_t:
        print(f"{county}: {county_vals_time_t[county]}")
    return county_vals_time_t
    
def analyze_CA_from_adj(adj_filename, data_type, start_day, end_day, dim, k = None, label_by_region = True, debug_mode = False, print_progress = False):
    CA_vy = CA_vy_from_adj(adj_filename, data_type, start_day, end_day, dim, debug_mode, print_progress)
    CA_vy.print_nontrivial_vines()
    CA_vy.plot(k, label_by_region)

def CA_vy_from_adj(adj_filename, data_type, start_day, end_day, dim, debug_mode = False, print_progress = False):
    county_map = PS.read_adj(adj_filename, debug_mode = debug_mode)
    county_names = list(county_map.R.keys())
    county_values = read_CA_county_data(county_names, data_type, start_day, end_day)
    CA_vy = county_map.toVineyard(county_values, dim, debug_mode, print_progress)
    return CA_vy
    
def read_CA_county_data(county_names, data_type, start_day, end_day = None, per_capita = True, avg_window = 14):
    '''
    county_names is the subset of CA counties that we're looking for in the usafacts file
    data_type: {'cumulative', 'new'}
    start_day and end_day input are in the format 'month/day/year' (e.g. 1/22/20, which is the earliest date in the file)
    Returns: Dictionary. Keys are county names. Values are lists of case counts for each day from start_day through end_day.
    '''
    if per_capita:
        file = xlrd.open_workbook('../data/CA_county_population_2018.xls')
        sheet = file.sheet_by_index(0)
        county_pop = {sheet.cell_value(i, 0) : sheet.cell_value(i, 1) for i in range(1, 59)}
    file = openpyxl.load_workbook('../data/covid_usafacts.xlsx')
    sheet = file.active
    start_idx = 5
    date_row = 1
    min_date_col = 5
    name_col = 2
    while not str(sheet.cell(date_row, start_idx).value).split(" ")[0] == start_day:
        start_idx += 1
    if end_day is None:
        time_cols = [start_idx]
    else:
        end_idx = start_idx
        while not str(sheet.cell(date_row, end_idx).value).split(" ")[0] == end_day:
            end_idx += 1
        time_cols = range(start_idx, end_idx + 1)
    CA_rows = range(193, 251)
    county_values = {}
    for i in CA_rows:
        sheet_countyname = sheet.cell(i, name_col).value
        if sheet_countyname == "City and County of San Francisco":
            county = 'San Francisco'
        else:
            county = str(sheet.cell(i, name_col).value).split()[:-1]
            county = " ".join(county)
        if county in county_names:
            cases = []
            for j in time_cols:
                val = sheet.cell(i, j).value
                if data_type == "new":
                    val -= sheet.cell(i, max(min_date_col, j - avg_window)).value
                    if county == "Imperial":
                        assert val >= 0, f"bad case count for county {county}, day {sheet.cell(date_row, j).value}"
                    val /= max(avg_window, j - min_date_col)
                if per_capita:
                    val /= county_pop[county]
                if end_day is None:
                    cases = val
                else:
                    cases.append(val)
            county_values.update({county : cases})
    return county_values

# South CA adjacency list saved as '../data/south_CA_county_adj.txt'
# South CA complex (filtration values) for March 1st through March 31st saved as 'south_CA_complex.txt'
# CA adjacency list saved as '../data/CA_county_adj.txt'
# CA shp file '../data/CA_counties/CA_counties_TIGER2016.shp' (found at https://data.ca.gov/dataset/ca-geographic-boundaries/resource/b0007416-a325-4777-9295-368ea6b710e6)
# 58 counties. Santa Barbara (2), Ventura (4), Los Angeles (5), San Francisco (10) are MultiPolygons.

data_type = input("Data type? [cumulative/new] ")
per_capita = (input("Per capita? [y/n] ") == 'y')

vineyard = (input("Calculate vineyard? [y/n] ") == 'y')
if vineyard:
    start_day = '2020-04-25'
    end_day = '2021-04-25'
    hom_dim = 1
    debug_mode = False
    print_progress = True
    label_by_region = True
    start_k = 0
    end_k = 5
    print("DEFAULT PARAMETERS: ")
    print("Start date: ", start_day)
    print("End date: ", end_day)
    print("Homological dimension: ", hom_dim)
    print("Vine indices to plot: ", start_k, " through ", end_k)
    print("Label vines by region: ", label_by_region)
    print("Debug mode: ", debug_mode)
    print("Print progress: ", print_progress)
    change_params = (input("Change default parameters? [y/n] ") == 'y')
    first_time = True
    while first_time or change_params:
        if change_params:
            is_subset = False
            if not first_time:
                is_subset = (input("Plot a subset of the previous dates? [y/n] ") == 'y')
            if is_subset:
                start_timestep = int(input("Start timestep (integer): "))
                end_timestep = int(input("End timestep (integer): "))
            else:
                start_day = input("Start date (format 'YYYY-MM-DD', earliest 2020-01-22: ")
                end_day = input("End date (format 'YYYY-MM-DD', latest 2021-05-03): ")
                debug_mode = (input("Debug mode? [y/n] ") == 'y')
                print_progress = (input("Print progress? [y/n] ") == 'y')
                hom_dim = int(input("Homology dimension: "))
            label_by_region = (input("Label vines by region? [y/n] ") == 'y')
            end_k = input("Vine index to end at? Enter integer or press enter to plot all: ")
            if len(end_k) == 0:
                end_k = None
            else:
                end_k = int(end_k)
                start_k = int(input("Vine index to start at? Enter integer: "))
        if first_time or not is_subset:
            county_ps = PS.read_adj('../data/CA_county_adj.txt', debug_mode = debug_mode)
            county_names = list(county_ps.R.keys())
            county_values = read_CA_county_data(county_names, data_type, start_day, end_day, per_capita = per_capita)
            CA_vy = county_ps.toVineyard(county_values, hom_dim, debug_mode, print_progress)
        legend = CA_vy.plot(start_k, end_k, label_by_region)
        save_legend = (input("Save legend? [y/n] ") == 'y')
        if save_legend:
            fname = f"CA_{data_type}_{start_day}_{end_day}"
            if end_k is not None:
                    if end_k is not None and start_k == 0:
                        fname += f"_top{end_k}"
                    elif end_k is not None and end_k < 0 and start_k == 0:
                        fname += f"_bottom{-end_k}"
                    else:
                        fname += f"_start{start_k}end{end_k}"
            else:
                fname += f"_start{start_k}"
            fname += ".pkl"
            with open(fname, "wb") as f:
                pickle.dump(legend, f)
        first_time = False
        change_params = (input("Change parameters and recalculate? [y/n] ") == 'y')

single_day = (input("Calculate PH for single day? [y/n] ") == 'y')
if single_day:
    day = '2020-06-03'
    filt = 'sublevel'
    annotate = False
    print("DEFAULT PARAMETERS: ")
    print("Date: ", day)
    print("Filtration type: ", filt)
    print("Annotate by region: ", annotate)
    if per_capita:
        suptitle = "CA COVID-19 Per Capita 14-Day Case Average by County"
    else:
        "CA COVID-19 14-Day Case Average by County"
    change_params = (input("Change default parameters? [y/n] ") == 'y')
    if change_params:
        day = input("Date (format 'month/day/year'): ")
        filt = input("Filtration type? [sub/super] ") + "level"
        annotate = (input("Annotate by region? [y/n] ") == 'y')
    ps = PS.read_adj("../data/CA_county_adj.txt")
    ps.triangulate()
    county_values = read_CA_county_data(list(ps.R.keys()), data_type, day)
    if filt == 'sublevel':
        sc = ps.sublevel_SC(county_values)
    else:
        sc = ps.superlevel_SC(county_values)
    sc.persistence(homology_coeff_field = 2)
    ps.plot_1d_finite_PD(sc, title = f"1D PH of {filt} filtration", suptitle = suptitle, annotate = annotate, figsize = 10)
    
plot = (input("Plot data (requires geopandas)?  [y/n] ") == 'y')
if plot:
    import vineyard.triangulate_map as tm
    day = input("Date (format 'month/day/year', e.g. 1/22/20): ")
    avg_window = input("Window size to average over: ")
    ca_shp = '../data/CA_counties/CA_counties_TIGER2016.shp'
    name_col = 'NAME'
    ca_gdf = tm.read_shp(ca_shp, name_col)
    ca_ps = PS.read_adj('../data/CA_county_adj.txt')
    county_names = list(ca_ps.R.keys())
    county_values = read_CA_county_data(county_names, data_type, day, per_capita = per_capita, avg_window = avg_window)
    if per_capita:
        tm.plot_attribute(ca_gdf, name_col, county_values)
    else:
        tm.plot_log_attribute(ca_gdf, name_col, county_values)