# -*- coding: utf-8 -*-
"""
LA non-vineyard figures
"""
import analyze_LA_nbhd as ala
import vineyard.triangulate_map as tm
from vineyard.polygonal_surface import PolygonalSurface as PS
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import geopandas as gpd
from collections import OrderedDict

import pickle

def plot(data_type, day, per_capita = True, avg_window = 14, exclude = None):
    nbhd_ps = PS.read_adj('../data/LA_nbhd_adj.txt')
    nbhd_names = list(nbhd_ps.R.keys())
    nbhd_values = ala.read_LA_data(nbhd_names, data_type, day, per_capita = per_capita, avg_window = avg_window)
    tm.plot_attribute(la_gdf, name_col, nbhd_values, exclude)
    
def legend(fname, ncol = 1):
    with open(fname, 'rb') as f:
        region_colors = pickle.load(f)
    new_region_colors = {}
    for region, color in region_colors.items():
        new_region = region.lower()
        symbs = [" ", "-", "/"]
        for symb in symbs:
            new_region = symb.join([s[0:1].upper()+s[1:] for s in new_region.split(symb)])
        new_region_colors.update({new_region : color})
    region_colors = new_region_colors
    sorted_region_colors = OrderedDict(sorted(region_colors.items()))
    plt.axis('off')
    patches = [mpatches.Patch(color= region_colors[region], label= region) for region in sorted_region_colors]
    plt.legend(handles=patches, ncol = ncol)
    
# Initial setup
la_shp = '../data/LA-shp/COVID19_by_Neighborhood.shp'
la_gdf = gpd.read_file(la_shp)
name_col = 'COMTY_NAME'

# Plot data
plot_data = (input("Plot data (requires geopandas)?  [y/n] ") == 'y')
if plot_data:
    data_type = 'new cases'
    per_capita = True
    day = '30-Jun'
    print("DEFAULT PARAMETERS: ")
    print("Data type: ", data_type)
    print("Per capita: ", per_capita)
    print("Day: ", day)
    change_params = (input("Change day? [y/n] ") == 'y')
    if change_params:
        day = input("Date (format 'day-month: ")
    exclude = ['ANGELES NATIONAL FOREST']
    plot(data_type, day, per_capita, exclude = exclude)
    
# Maps to go with the vineyards
map_vy_legend = (input("Make map to go along with vineyard legend? [y/n] ") == 'y')
if map_vy_legend:
    fname = input("File name: ")
    with open(fname, 'rb') as f:
        region_colors = pickle.load(f)
    tm.plot_legend(la_gdf, name_col, region_colors)
    new_region_colors = {}
    for region, color in region_colors.items():
        new_region = region.lower()
        symbs = [" ", "-", "/"]
        for symb in symbs:
            new_region = symb.join([s[0:1].upper()+s[1:] for s in new_region.split(symb)])
        new_region_colors.update({new_region : color})
    region_colors = new_region_colors
    plt.axis('off')
    patches = [mpatches.Patch(color= region_colors[region], label= region) for region in region_colors]
    ncol = 1
    plt.legend(handles=patches, ncol = ncol)

mk_legend = (input("Make legend? [y/n] ") == 'y')
if mk_legend:
    fname = input("File name: ")
    ncol = 5
    legend(fname, ncol)
    
