# -*- coding: utf-8 -*-
"""
Testing PolygonalSurface class
"""

from vineyard.polygonal_surface import PolygonalSurface as PS
import matplotlib.pyplot as plt
import networkx as nx
import gudhi as gd

# Testing add_region
ps = PS()
ps.add_region('A', ['B', 'C', 'D'])
ps.add_region('B', ['A', 'E', 'F'])
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.G.nodes)
print(ps.R)
print(ps.node_adj_R)
print(ps.G.edges.data())
print(ps.get_adj_rgns([0, 1]))
print(ps.get_adj_rgns([0, 2]))
print(ps.get_adj_rgns([0, 5]))

# Testing ps.expand()
ps.expand('A', 2)
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.R)
print(ps.node_adj_R)
print(ps.G.edges.data())

# testing exterior key in R
ps= PS()
print(ps.R)
ps.add_region('A', ['exterior', 'C', 'B'])
ps.add_region('B', ['A', 'D', 'exterior'])
ps.add_region('C', ['exterior', 'exterior', 'A'])
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.R)
print(ps.node_adj_R)
print(ps.G.edges.data())
print(ps.get_adj_rgns([0, 1]))

# Testing create_from_dict_of_adj
adj = {'A': ['B', 'exterior', 'exterior'], 'B': ['C', 'A', 'D']}
ps = PS.create_from_dict_of_adj(adj)
ps.compare_true_adj(adj)
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.R)
print(ps.node_adj_R)
print(ps.G.edges.data())

# Testing on regions with disconnected boundary intersection
Koreatown = ['Wilshire', 'Bangladesh', 'Wilshire', 'Pico-Union', 'Harvard Heights', 'Country Club Park', 'Hancock Park']
Wilshire = ['Bangladesh', 'Bangladesh', 'Koreatown', 'Hancock Park', 'exterior', 'Pico-Union', 'Koreatown']
adj = {'Koreatown' : Koreatown, 'Wilshire' : Wilshire}
ps = PS.create_from_dict_of_adj(adj)
ps.compare_true_adj(adj)
nx.draw_planar(ps.G, with_labels = True)
plt.show()

#Testing PolygonalSurface when some regions have repeated edges
ps = PS()
ps.add_region('A', ['B', 'B', 'exterior'])
ps.add_region('B', ['exterior', 'A', 'A', 'C'])
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.R)
print(ps.node_adj_R)
print(ps.G.edges.data())

# Testing interior regions
west_vernon = ['EXPOSITION PARK', 'SOUTH PARK', 'HARVARD PARK', 'HYDE PARK', 'LEIMERT PARK']
vermont = ['WEST VERNON']
other_int = ['WEST VERNON']
adj = {'WEST VERNON' : west_vernon, 'VERMONT SQUARE' : vermont, 'other int' : other_int}
ps = PS.create_from_dict_of_adj(adj)
nx.draw_planar(ps.G, with_labels = True)
plt.show()
ps.compare_true_adj(adj)
for n in ps.G.nodes():
    print(f"{n}: {ps.node_adj_R[n]}")
ps.triangulate()
print(ps.E)
print(ps.T)

# Testing interior region that intersects outer region's outer boundary
outer = ['nbr1', 'nbr2', 'vertex:inner', 'nbr2', 'nbr3']
inner = ['vertex:nbr2', 'outer']
adj = {'outer' : outer, 'inner' : inner}
ps = PS.create_from_dict_of_adj(adj, debug_mode = True)
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.R)
ps.triangulate()
print("\nEdges:\n", ps.E)
print("\nTriangles:\n", ps.T)

outer = ['nbr1', 'nbr2', 'vertex:inner', 'nbr2', 'nbr3']
inner = ['vertex:nbr2', 'outer']
adj = {'inner' : inner, 'outer' : outer}
ps = PS.create_from_dict_of_adj(adj, debug_mode = True)
nx.draw_planar(ps.G, with_labels = True)
plt.show()
print(ps.R)

# Test 4 corners
valley_glen = ['NORTH HOLLYWOOD', 'vertex:VALLEY VILLAGE', 'SHERMAN OAKS', 'VAN NUYS']
north_hollywood = ['exterior', 'TOLUCA TERRACE', 'TOLUCA WOODS', 'TOLUCA LAKE', 'exterior', 'STUDIO CITY', 'VALLEY VILLAGE', 'vertex:SHERMAN OAKS', 'VALLEY GLEN', 'VAN NUYS', 'PANORAMA CITY', 'SUN VALLEY']
valley_village = ['STUDIO CITY', 'SHERMAN OAKS', 'vertex:VALLEY GLEN', 'NORTH HOLLYWOOD']
sherman_oaks = ['VALLEY GLEN', 'vertex:NORTH HOLLYWOOD', 'VALLEY VILLAGE', 'STUDIO CITY', 'BEVERLY CREST', 'BEL AIR', 'ENCINO', 'LAKE BALBOA', 'VAN NUYS']
adj = {'VALLEY GLEN' : valley_glen, 'NORTH HOLLYWOOD' : north_hollywood, 'VALLEY VILLAGE' : valley_village, 'SHERMAN OAKS' : sherman_oaks}
ps = PS.create_from_dict_of_adj(adj)
nx.draw_planar(ps.G, with_labels = True)
plt.show()
ps.compare_true_adj(adj)

# Fixing exterior filtration bug
rockaway_ps = PS.read_adj('rockaway_adj.txt')
rockaway_vals = {'11697' : 620, '11694' : 1972, '11693' : 573, '11692': 791, '11691': 2409}
for u, v in rockaway_ps.G:
    print(rockaway_ps.G[u][v]['name'])
rockaway_sc = rockaway_ps.sublevel_SC(rockaway_vals)
ph = rockaway_sc.persistence()
gd.plot_persistence_diagram(persistence = ph)