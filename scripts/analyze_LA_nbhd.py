# -*- coding: utf-8 -*-
"""
Analyze LA neighborhood covid data using vineyards.
"""
import sys
sys.path.append('C:\\Users\\abiga')
from vineyard.polygonal_surface import PolygonalSurface as PS
import openpyxl
import pickle
    
def read_LA_data(nbhd_names, data_type, start_day, end_day = None, per_capita = True, avg_window = 14):
    '''
    Parameters
    ----------
    nbhd_names : list of Strings.
        Nbhd names that we're looking for in Faith's file.
    data_type : String, with options {'all cases', 'new cases', 'all deaths', 'new deaths'} or anything in column "Data Type" in Faith's file.
    start_day, end_day : String.
        In the format day-month. Month is represented by its first 3 letters, 
        with first letter capitalized (e.g. 17-Mar, which is the first day, or 
        3-Jan, which is the last day). Some restrictions on dates depending on data_type.

    Returns
    -------
    nbhd_values : Dictionary.
        Key is nbhd name, value is list of the data_type value of that nbhd for times from start_day through end_day

    '''
    nbhd_names = {nbhd.lower().split("_")[0] for nbhd in nbhd_names}
    nbhd_names.remove('exterior')
    
    file = openpyxl.load_workbook('../data/LA_nbhd_covid.xlsx')
    sheet = file.active
    start_time_idx = 4
    pop_col = 3
    min_date_col = 4
    date_row = 3
    while not sheet.cell(date_row, start_time_idx).value == start_day:
        start_time_idx += 1
      
    end_time_idx = start_time_idx
    if end_day is not None:
        while not sheet.cell(date_row, end_time_idx).value == end_day:
            end_time_idx += 1
    time_cols = range(start_time_idx, end_time_idx + 1)
    
    if data_type == 'all cases' or data_type == 'new cases':
        row_idx = 4
        data_type = 'Cumulative Confirmed Case Count'
    else:
        row_idx = 561
        data_type = 'Cumulative Death Count'

    def convert_to_int(x):
        if isinstance(x, str):
            x = int(list(x)[-1])
        return x
    
    nbhd_values = {}
    while sheet.cell(row_idx, 2).value == data_type:
        nbhd = sheet.cell(row_idx, 1).value
        if nbhd.lower() in nbhd_names:
            data_by_time = []
            for j in time_cols:
                val = convert_to_int(sheet.cell(row_idx, j).value)
                if data_type == 'new cases' or data_type == 'new deaths':
                    val -= sheet.cell(row_idx, max(min_date_col, j - avg_window)).value
                    val /= max(j-min_date_col, avg_window)
                if per_capita:
                    pop = sheet.cell(row_idx, pop_col).value
                    val /= pop
                if end_day is None:
                    data_by_time = val
                else:
                    data_by_time.append(val)
            nbhd_values.update({nbhd.upper() : data_by_time})
        row_idx += 1
    return nbhd_values

if __name__ == "__main__":
    vineyard = (input("Calculate vineyard? [y/n] ") == 'y')
    if vineyard:
        data_type = 'new cases'
        per_capita = True
        start_day = '25-Apr'
        end_day = '25-Apr2'
        start_k = 20
        end_k =  None
        label_by_region = True
        debug_mode = False
        print_progress = True
        start_timestep = 0
        end_timestep = None
        show_legend = False
        print("DEFAULT PARAMETERS: ")
        print("Data type: ", data_type)
        print("Per capita: ", per_capita)
        print("Start date: ", start_day)
        print("End date: ", end_day)
        print("Vine indices to plot: ", start_k, " through ", end_k)
        print("Label vines by region: ", label_by_region)
        print("Debug mode: ", debug_mode)
        print("Print progress: ", print_progress)
        print("Show legend: ", show_legend)
        change_params = (input("Change default parameters? [y/n] ") == 'y')
        first_time = True
        while first_time or change_params:
            if change_params:
                is_subset = False
                if not first_time:
                    is_subset = (input("Plot a subset of the previous dates? [y/n] ") == 'y')
                if is_subset:
                    start_timestep = int(input("Start timestep (integer): "))
                    end_timestep = int(input("End timestep (integer): "))
                else:
                    data_type = input("Data type? [all cases/new cases/all deaths/new deaths] ")
                    per_capita = (input("Per capita? [y/n] ") == 'y')
                    start_day = input("Start date (format 'day-month'): ")
                    end_day = input("End date (format 'day-month'): ") 
                    debug_mode = (input("Debug mode? [y/n] ") == 'y')
                    print_progress = (input("Print progress? [y/n] ") == 'y')
                show_legend = (input("Show legend? [y/n] ") == 'y')
                end_k = input("Vine index to end at? Enter integer or press enter to plot all: ")
                if len(end_k) == 0:
                    end_k = None
                else:
                    end_k = int(end_k)
                    start_k = int(input("Vine index to start at? Enter integer: "))
                label_by_region = (input("Label vines by region? [y/n] ") == 'y')
            if first_time or not is_subset:
                nbhd_ps = PS.read_adj('../data/LA_nbhd_adj.txt', debug_mode = debug_mode)
                nbhd_names = list(nbhd_ps.R.keys())
                nbhd_values = read_LA_data(nbhd_names, data_type, start_day, end_day, per_capita = per_capita)
                LA_vy = nbhd_ps.toVineyard(nbhd_values, 1, debug_mode = debug_mode, print_progress = print_progress)
            exclude = ['ANGELES NATIONAL FOREST']
            LA_vy.print_vines(LA_vy.nontrivial_finite_vines(start_k, end_k))
            legend = LA_vy.plot(start_k = start_k, end_k = end_k, label_by_region = label_by_region, 
                                show_legend = show_legend, start_timestep = start_timestep, 
                                end_timestep = end_timestep,
                                exclude = exclude, 
                                #region_colors = '../figures/LA/LA_newcases_25-Apr_25-Apr2excludeAngeles_top6.pkl'
                                region_colors = '../figures/LA/LA_newcases_25Apr2020_25Apr2021_excludeforest.pkl'
                                )
            save_legend = (input("Save legend? [y/n] ") == 'y')
            if save_legend:
                dataname = "".join(data_type.split(" "))
                fname = f"LA_{dataname}_{start_day}_{end_day}"
                for rgn in exclude:
                    fname += f"exclude{rgn}"
                if end_k is not None:
                    if end_k is not None and start_k == 0:
                        fname += f"_top{end_k}"
                    elif end_k is not None and end_k < 0 and start_k == 0:
                        fname += f"_bottom{-end_k}"
                    else:
                        fname += f"_start{start_k}end{end_k}"
                else:
                    fname += f"_start{start_k}"
                
                fname += ".pkl"
                print(fname)
                with open(fname, "wb") as f:
                    pickle.dump(legend, f)
            first_time = False
            change_params = (input("Change parameters and recalculate? [y/n] ") == 'y')
    
    single_day = (input("Calculate PH for single day? [y/n] ") == 'y')
    if single_day:
        data_type = 'new cases'
        per_capita = True
        day = '30-Jun'
        filt = 'sublevel'
        annotate = False
        print("DEFAULT PARAMETERS: ")
        print("Data type: ", data_type)
        print("Per capita: ", per_capita)
        print("Date: ", day)
        print("Filtration type: ", filt)
        print("Annotate by region: ", annotate)
        
        change_params = (input("Change default parameters? [y/n] ") == 'y')
        first_time = True
        while first_time or change_params:
            if change_params:
                data_type = input("Data type? [all cases/new cases/all deaths/new deaths] ")
                per_capita = (input("Per capita? [y/n] ") == 'y')
                day = input("Date (format 'day-month': ")
                filt = input("Filtration type? [sub/super] ") + "level"
                annotate = (input("Annotate by region? [y/n] ") == 'y')
            ps = PS.read_adj('../data/LA_nbhd_adj.txt')
            nbhd_names = list(ps.R.keys())
            nbhd_values = read_LA_data(nbhd_names, data_type, day)
            if filt == 'sublevel':
                sc = ps.sublevel_SC(nbhd_values)
            else:
                sc = ps.superlevel_SC(nbhd_values)
            if per_capita:
                suptitle = "LA COVID-19 14-Day Average Per Capita Case Rate by Neighborhood"
            else:
                suptitle = "LA COVID-19 14-Day Average Case Rate by Neighborhood"
            sc.persistence(homology_coeff_field = 2)
            exclude = ['ANGELES NATIONAL FOREST']
            paper_format = (input("Paper format? [y/n] ") == 'y')
            if paper_format:
                title = None
                suptitle = None
            else:
                title = f"1D PH of {filt} filtration"
            ps.plot_1d_finite_PD(sc, title = title, suptitle = suptitle, annotate = annotate, figsize = 5, exclude = exclude)
            first_time = False
            change_params = (input("Change parameters and recalculate? [y/n] ") == 'y')