# -*- coding: utf-8 -*-
"""
Test vineyard corner case
"""
from vineyard.vineyard import Simplex
from vineyard.vineyard import Vineyard

simplices = [Simplex([i], [i, -i]) for i in range(10)] + [Simplex([i], [i, -i]) for i in range(10)] 
vy = Vineyard(simplices, 1)
vy.print_curr_simplices()   # This should be [9], [9], [8], [8], ..., [0], [0]