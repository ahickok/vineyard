# -*- coding: utf-8 -*-
"""
Created on Fri May 14 11:53:04 2021

@author: abiga
"""
import matplotlib.pyplot as plt
import math
import numpy as np

big_font = (input("Big font? [y/n] ") == 'y')
if big_font:
    plt.rcParams.update({'font.size': 25})
eps = .07
plt.figure(figsize = (2, 2))
plt.axis('off')
vertices = np.array([[math.cos(2*math.pi*k/4), math.sin(2*math.pi*k/4)] for k in range(4)])
for i, v in enumerate(vertices):
    if i == 2 or i == 3:
        plt.text(v[0] - eps, v[1] - eps, f"{i}", horizontalalignment = 'right', verticalalignment = 'top')
    else:
        plt.text(v[0]+eps, v[1]+eps, f"{i}")
plt.scatter(vertices[:, 0], vertices[:, 1], color = 'k')
if big_font:
    plt.savefig("../figures/FSC_big/fsc1.png", bbox_inches = 'tight')
else:
    plt.savefig("../figures/FSC/fsc1.png", bbox_inches = 'tight')
plt.show()

plt.figure(figsize = (2, 2))
plt.axis('off')
for i, v in enumerate(vertices):
    if i == 2 or i == 3:
        plt.text(v[0] - eps, v[1] - eps, f"{i}", horizontalalignment = 'right', verticalalignment = 'top')
    else:
        plt.text(v[0]+eps, v[1]+eps, f"{i}")
plt.scatter(vertices[:, 0], vertices[:, 1], color = 'k')
plt.plot(vertices[:, 0], vertices[:, 1], color = 'k')
if big_font:
    plt.savefig("../figures/FSC_big/fsc2.png", bbox_inches = 'tight')
else:
    plt.savefig("../figures/FSC/fsc2.png", bbox_inches = 'tight')
plt.show()

plt.figure(figsize = (2, 2))
plt.axis('off')
plt.scatter(vertices[:, 0], vertices[:, 1], color = 'k')
for i, v in enumerate(vertices):
    if i == 2 or i == 3:
        plt.text(v[0] - eps, v[1] - eps, f"{i}", horizontalalignment = 'right', verticalalignment = 'top')
    else:
        plt.text(v[0]+eps, v[1]+eps, f"{i}")
plt.scatter(vertices[:, 0], vertices[:, 1], color = 'k')
vertices = np.vstack((vertices, vertices[0, :]))
plt.plot(vertices[:, 0], vertices[:, 1], color = 'k')
if big_font:
    plt.savefig("../figures/FSC_big/fsc3.png", bbox_inches = 'tight')
else:
    plt.savefig("../figures/FSC/fsc3.png", bbox_inches = 'tight')
plt.show()

plt.figure(figsize = (2, 2))
plt.axis('off')
for i, v in enumerate(vertices[:-1]):
    if i == 2 or i == 3:
        plt.text(v[0] - eps, v[1] - eps, f"{i}", horizontalalignment = 'right', verticalalignment = 'top')
    else:
        plt.text(v[0]+eps, v[1]+eps, f"{i}")
tri1 = np.array([vertices[k, :] for k in range(3)])
plt.fill(tri1[:, 0], tri1[:, 1], edgecolor = 'k', facecolor = 'darkgray')
plt.scatter(vertices[:, 0], vertices[:, 1], color = 'k')
plt.plot(vertices[:, 0], vertices[:, 1], color = 'k')
if big_font:
    plt.savefig("../figures/FSC_big/fsc4.png", bbox_inches = 'tight')
else:
    plt.savefig("../figures/FSC/fsc4.png", bbox_inches = 'tight')
plt.show()

plt.figure(figsize = (2, 2))
plt.axis('off')
for i, v in enumerate(vertices[:-1]):
    if i == 2 or i == 3:
        plt.text(v[0] - eps, v[1] - eps, f"{i}", horizontalalignment = 'right', verticalalignment = 'top')
    else:
        plt.text(v[0]+eps, v[1]+eps, f"{i}")
plt.fill(tri1[:, 0], tri1[:, 1], edgecolor = 'k', facecolor = 'darkgray')
tri2 = np.array([vertices[0, :], vertices[2, :], vertices[3, :]])
plt.fill(tri2[:, 0], tri2[:, 1], edgecolor = 'k', facecolor = 'darkgray')
plt.scatter(vertices[:, 0], vertices[:, 1], color = 'k')
plt.plot(vertices[:, 0], vertices[:, 1], color = 'k')
if big_font:
    plt.savefig("../figures/FSC_big/fsc5.png", bbox_inches = 'tight')
else:
    plt.savefig("../figures/FSC/fsc5.png", bbox_inches = 'tight')
plt.show()