# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 08:54:36 2021

@author: abiga
"""
from PIL import Image
import math
from vineyard.vineyard import Simplex
from vineyard.vineyard import Vineyard

def coord_to_idx(coord, N):
    # N is the number of rows in the image.
    # Vertices are the vertices of all the pixels (row 0 is the top of the top row of pixels, row N is the bottom of the bottom rows of pixels)
    # Indexing starts at top left and goes row by row. Goes top to bottom and left to right.
    n = coord[0]
    m = coord[1]
    idx = n*N + m
    return idx

def get_filt_from_adj(adj_px, px_vals, r_steps):
    # adj_px is a list of adjacent pixels, where each pixel is represented as a tuple (n, m)
    # px_vals[(n,m)] = (gray_val, r) where gray_val = grayscale value of the pixel and r = r_step where that pixel first appears
    adj_px_vals = [px_vals[px] for px in adj_px]
    adj_px_vals.sort(key = lambda px_val : px_val[1])   # sort by r value
    r_stepsize = r_steps[1] - r_steps[0]
    
    # Get rid of duplicate r vals. If two px have same r val, only keep the one with the smaller gray val
    vals_with_unique_rs = []
    min_gray = adj_px_vals[0][0]    # gray value of first px in adj_px_vals
    r = adj_px_vals[0][1]   # r value of first px in adj_px_vals
    for i, px_val in enumerate(adj_px_vals[1:]):
        if px_val[1] == r:
            min_gray = min(min_gray, px_val[0])
        else:
            vals_with_unique_rs.append((min_gray, r))
            r = px_val[1]
            min_gray = min(min_gray, px_val[0])
    vals_with_unique_rs.append((min_gray, r))
    
    filt_seq = []
    curr_r_step = 0
    curr_gray_val = 255
    for val in vals_with_unique_rs:
        next_r_step = val[1]
        for r_step in range(curr_r_step, next_r_step, r_stepsize):
            filt_seq.append(curr_gray_val)
        curr_gray_val = min(curr_gray_val, val[0])
        curr_r_step = next_r_step
    last_r_step = r_steps[-1]
    for r_step in range(curr_r_step, last_r_step + r_stepsize, r_stepsize):
        filt_seq.append(curr_gray_val)
    return filt_seq

def rgba_to_grayscale(rgba):
    # see Grayscale wikipedia page, converting color to grayscale
    r = rgba[0]
    g = rgba[1]
    b = rgba[2]
    return .2126*r + .7152*g + .0722*b

def dist_to_center(idx, center):
    x_c = center[0]
    y_c = center[1]
    n = idx[0]
    m = idx[1]
    return math.sqrt((n- x_c)**2 + (m-y_c)**2)

im = Image.open('../data/snowflake/snowflake1.png')
N, M = im.size
center = (N/2, M/2)
max_r = dist_to_center((0,0), center)
px_vals = {}

r_stepsize = 50
r_steps = [i for i in range(0, int(max_r - max_r%r_stepsize + 2*r_stepsize), r_stepsize)]
print(r_steps)
for n in range(N):
    for m in range(M):
        r = dist_to_center((n, m), center)
        r_step = int(r - r%r_stepsize + r_stepsize) # the first r_step >= r
        gray_val = rgba_to_grayscale(im.getpixel((n,m)))
        px_vals.update({(n,m) : (gray_val, r_step)})
print("calculated pixel grayscale and r_step values")

simplices = []
for n in range(N):
    print("n = ", n)
    for m in range(M):
        # Get vertices of the pixel
        top_left = coord_to_idx((n, m), N)
        top_right = coord_to_idx((n, m+1), N)
        bot_left = coord_to_idx((n+1, m), N)
        bot_right = coord_to_idx((n+1, m+1), N)
        
        # Append the 2-simplices (triangles)
        filt = [255 if r_step < px_vals[(n,m)][1] else px_vals[(n,m)][0] for r_step in r_steps]
        nodes = [top_left, top_right, bot_left]
        simplices.append(Simplex(nodes, filt))
        nodes = [top_right, bot_left, bot_right]
        simplices.append(Simplex(nodes, filt))

        # Add the diagonal edge
        diag_nodes = [bot_left, top_right]
        simplices.append(Simplex(diag_nodes, px_vals[(n,m)]))
        
        # Add the right edge
        right_nodes = [top_right, bot_right]
        if m == M-1:
            adj_px = [(n,m)]
        else:
            adj_px = [(n, m), (n, m+1)]
        filt = get_filt_from_adj(adj_px, px_vals, r_steps)
        simplices.append(Simplex(right_nodes, filt))
        
        # Add the bottom edge
        bot_nodes = [bot_left, bot_right]
        if n == N-1:
            adj_px = [(n, m)]
        else:
            adj_px = [(n, m), (n+1, m)]
        filt = get_filt_from_adj(adj_px, px_vals, r_steps)
        simplices.append(Simplex(bot_nodes, filt))

        # Add bottom right node
        if n == N-1 and m == M-1:
            adj_px = [(n, m)]
        elif n == N-1:
            adj_px = [(n, m), (n, m+1)]
        elif m == M-1:
            adj_px = [(n, m), (n+1, m)]
        else:
            adj_px = [(n, m), (n, m+1), (n+1, m), (n+1, m+1)]
        filt = get_filt_from_adj(adj_px, px_vals, r_steps)
        simplices.append(Simplex([bot_right], filt))
        
        if n == 0 and m == 0:
            # Add top left node
            filt = [255 if r_step < px_vals[(0,0)][1] else px_vals[(0,0)][0] for r_step in r_steps]
            simplices.append(Simplex([top_left], filt))
        if n == 0:
            # Add top edge
            top_nodes = [top_left, top_right]
            filt = [255 if r_step < px_vals[(0,m)][1] else px_vals[(0,m)][0] for r_step in r_steps]
            simplices.append(Simplex(top_nodes, filt))
            
            # Add top right node
            if m == M-1:
                adj_px = [(n,m)]
            else:
                adj_px = [(n, m), (n, m+1)]
            filt = get_filt_from_adj(adj_px, px_vals, r_steps)
            simplices.append(Simplex([top_right], filt))
        if m == 0:
            # Add left edge
            left_nodes = [top_left, bot_left]
            filt = [255 if r_step < px_vals[(n,m)][1] else px_vals[(n,m)][0] for r_step in r_steps]
            simplices.append(Simplex(left_nodes, filt))
            
            # Add bottom left node
            if n == N-1:
                adj_px = [(n, m)]
            else:
                adj_px = [(n, m), (n+1, m)]
                filt = get_filt_from_adj(adj_px, px_vals, r_steps)
                simplices.append(Simplex([bot_left], filt))

snow_vy = Vineyard(simplices, 1, print_progress = True)
snow_vy.plot()