# -*- coding: utf-8 -*-
'''
Turn shp file into PolygonalSurface
'''
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import gudhi as gd
import math
from shapely.geometry import Point
from shapely.geometry import LineString

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

def analyze_sublevel_SC(polygonal_map, region_values, t):
    # Only for debugging
    sublevel_sc = polygonal_map.sublevel_SC(region_values, t)
    
    # Check the simplices
    for spx in sublevel_sc.get_filtration():
        print(spx)
    print("")
        
    # Check that the filtration is non-decreasing
    print(sublevel_sc.make_filtration_non_decreasing())    # should return False
    print("")
    
    # Compute PH (required for birth/death pairs and calculate/plot PH below)
    sublevel_sc.compute_persistence(homology_coeff_field = 2)
    
    # Print birth/death pairs
    pairs = sublevel_sc.persistence_pairs()
    for pair in pairs: print(pair)
    print("")
    
    # Calculate/plot ph
    ph = sublevel_sc.persistence_intervals_in_dimension(1)
    print(ph)
    gd.plot_persistence_diagram(persistence = ph)
    
def analyze_filtration_values(polygonal_map, region_values, t):
    # Only for debugging
    node_values, edge_values, tri_values = polygonal_map.filtration_values(region_values)
    for n in node_values:
        print(f"{n}: {node_values[n][t]}")
    for e in edge_values:
        print(f"{e}: {edge_values[e][t]}")
    for tri in tri_values:
        print(f"{tri}: {tri_values[tri][t]}")
    return node_values, edge_values, tri_values

def analyze_triangulation(polygonal_map):
    # only for debugging
    polygonal_map.triangulate()
    E = polygonal_map.E
    T = polygonal_map.T
    print("Edges")
    for region in E:
        print(f"{region}: {E[region]}")
    print("\nTriangles")
    for region in T:
        print(f"{region}: {T[region]}")

def compare_adj(adj1, adj2, gdf, name_col):
    for rgn in adj1:
        match = True
        rgn_adj1 = adj1[rgn]
        rgn_adj2 = adj2[rgn]
        N = len(rgn_adj1)
        if N != len(rgn_adj2):
            match = False
        elif len(rgn_adj1) == 1:
            if rgn_adj1[0] != rgn_adj2[0]:
                match = False
        else:
            # Set first_rgn equal to first non-exterior adjacency in adj1[rgn] that has has a connected intersection with rgn
            adj1_idx = 0
            while adj1_idx < N and rgn_adj1.count(rgn_adj1[adj1_idx]) > 1:
                adj1_idx += 1
            first_rgn = rgn_adj1[adj1_idx]
            # find index for first_rgn in adj2
            adj2_idx = 0
            while adj2_idx < N and rgn_adj2[adj2_idx] != first_rgn:
                adj2_idx += 1
            # compare the two lists element-wise
            for i in range(N):
                if not rgn_adj1[(adj1_idx + i) % N] == rgn_adj2[(adj2_idx + i)%N]:
                    match = False
                    break
        if not match:
            plot_region_with_nbrs(rgn, gdf, name_col)
            plt.show()

def find_neighbors(gdf, eps = 3e-5, shpfile = None, recompute = False, verbose = True):
    if 'Neighbors' in gdf.columns and not recompute:
        if verbose: print("reading neighbors")
        for i, rgn in gdf.iterrows():
            if rgn['Neighbors'] is None:
                nbrs_as_list = []
            else:
                nbrs_as_list = [int(nbr) for nbr in rgn['Neighbors'].split("\t") if len(nbr) > 0]
            gdf['Neighbors'][i] = nbrs_as_list
        return gdf
    else:
        if recompute and 'Neighbors' in gdf.columns:
            del gdf['Neighbors']
        if verbose: print("computing neighbors")
        nbr_lists = []
        for i, rgn in gdf.iterrows():
            if verbose: print(i)
            rgn_nbr_indices = []
            for j, nbr in gdf.iterrows():
                if j != i and nbr['geometry'].distance(rgn['geometry']) < eps:
                    if verbose: print("adding nbr ", j)
                    rgn_nbr_indices.append(j)
            nbr_lists.append(rgn_nbr_indices)
        gdf['Neighbors'] = nbr_lists
        if shpfile is not None:
            save_neighbors(gdf, shpfile)
        return gdf

def get_adj(gdf, name_col):
    num_regions = len(gdf.geometry)
    adj = {gdf[name_col][i] : get_adj_of_region(i, gdf, name_col) for i in range(num_regions)}
    return adj
        
def get_adj_of_region(rgn_idx, gdf, name_col, max_gap_size = 5e-4, min_nbr_len = 1e-3, min_ext_len = 5e-4, cos_tol = .02):
    rgn_bdry = gdf['geometry'][rgn_idx].exterior.coords
    N = len(rgn_bdry)
    avail_rgn_edges = {(rgn_bdry[i], rgn_bdry[(i+1)%N]) for i in range(N) if Point(rgn_bdry[i]).distance(Point(rgn_bdry[(i+1)%N])) > 0}  # use a set because set removal is O(1)
    uv_lists = {edge : [] for edge in avail_rgn_edges}
    if not 'Neighbors' in gdf.columns:
        gdf = find_neighbors(gdf)
    rgn_nbrs = gdf['Neighbors'][rgn_idx]
    if len(rgn_nbrs) == 0:
        return ['exterior']
    for nbr in rgn_nbrs:
        nbr_added = False
        nbr_bdry = gdf['geometry'][nbr].exterior.coords
        N_nbr = len(nbr_bdry)
        nbr_edges = [LineString([nbr_bdry[i], nbr_bdry[(i+1)%N_nbr]]) for i in range(N_nbr)]
        for nbr_edge in nbr_edges:
            if nbr_edge.length > 0:
                possible_targets = [rgn_edge for rgn_edge in avail_rgn_edges if nbr_edge.distance(LineString(rgn_edge)) < max_gap_size]
                #print("number possible targets: ", len(possible_targets))
                for rgn_edge in possible_targets:
                    intersection_type, points = get_intersection_info(nbr_edge, LineString(rgn_edge), cos_tol)
                    dist = gdf['geometry'][nbr].distance(LineString(rgn_edge))
                    if intersection_type == "1d":
                        rgn_edge_len = LineString(rgn_edge).length
                        
                        update_uvlist(uv_lists[rgn_edge], points, nbr, dist)
                        nbr_added = True
                        if abs(points[0])<1e-10 and abs(points[1]-rgn_edge_len)<1e-10 and Point(nbr_edge.coords[0]).distance(LineString(rgn_edge))<1e-10 and Point(nbr_edge.coords[1]).distance(LineString(rgn_edge))<1e-10:
                            avail_rgn_edges.remove(rgn_edge)
                    if intersection_type == "0d" and points is not None and dist < 1e-10:
                        # Only add to uvlist if dist is very close to 0 (that is, nbr really is adjacent)
                        nbr_added = True
                        update_uvlist(uv_lists[rgn_edge], points, nbr, dist)
        if not nbr_added:
            # This can happen if nbr and rgn are adjacent via a shared vertex, but the edges adjacent to the vertex have already been removed from the available edge list
            for i in range(N):
                dist = gdf['geometry'][nbr].distance(Point(rgn_bdry[i]))
                if dist <1e-10:
                    update_uvlist(uv_lists[(rgn_bdry[i], rgn_bdry[(i+1)%N])], [0, 0], nbr, dist)
                    break

    adj_list = []
    rgn_edges = [(rgn_bdry[i], rgn_bdry[(i+1)%N]) for i in range(N) if Point(rgn_bdry[i]).distance(Point(rgn_bdry[(i+1)%N])) >0]
    ext_len = 0
    nbr_len = 0
    curr_nbr = None
    curr_nbr_gap = None
    recent_edge = None
    for edge in rgn_edges:
        edge_len = LineString(edge).length
        uv_list = uv_lists[edge]
        uv_list.sort(key = lambda uv : (uv[0], uv[1]))
        if len(uv_list) > 1: print("\n", uv_list, "\n")
        prev_node = 0
        for uv in uv_list:
            if curr_nbr != uv[2]:
                if curr_nbr is not None:
                    if recent_edge != curr_nbr:
                        # This means curr_nbr's length must have been too short to get counted as an edge
                        if curr_nbr_gap <1e-10:
                            adj_list.append(f"vertex:{curr_nbr}")
                nbr_len = 0
                curr_nbr_gap = uv[3]
                curr_nbr = uv[2]
            elif curr_nbr is not None:
                curr_nbr_gap = min(curr_nbr_gap, uv[3])
            if uv[0] > prev_node:
                ext_len += uv[0] - prev_node
                if ext_len > min_ext_len and (len(adj_list)==0 or adj_list[-1] != 'exterior'):
                    if recent_edge != curr_nbr and curr_nbr is not None and curr_nbr_gap < 1e-10:
                        adj_list.append(f"vertex:{curr_nbr}")
                    adj_list.append('exterior')
                    recent_edge = 'exterior'
                    curr_nbr = None
                    curr_nbr_gap = None
            nbr_len += min(edge_len, uv[1]) - max(0, uv[0])
            if (nbr_len > min_nbr_len or (min(edge_len, uv[1])-max(0, uv[0])) > .9*edge_len) and (len(adj_list) == 0 or adj_list[-1] != uv[2]):
                adj_list.append(uv[2])
                recent_edge = uv[2]
            ext_len = 0
            prev_node = uv[1]
        if edge_len > prev_node:
            ext_len += edge_len - prev_node
            if ext_len > min_ext_len and (len(adj_list)==0 or adj_list[-1] != 'exterior'):
                if recent_edge != curr_nbr and curr_nbr is not None and curr_nbr_gap < 1e-10:
                    adj_list.append(f"vertex:{curr_nbr}")
                adj_list.append('exterior')
                recent_edge = 'exterior'
                curr_nbr = None
                curr_nbr_gap = None
    if curr_nbr is not None and curr_nbr_gap < 1e-10 and recent_edge != curr_nbr:
        adj_list.append(f"vertex:{curr_nbr}")
    if len(rgn_nbrs) == 1 and gdf['geometry'][rgn_idx].distance(gdf['geometry'][rgn_nbrs[0]]) == 0 and len(adj_list) <= 1:
        adj_list = [rgn_nbrs[0]]
    adj_names = []
    for adj in adj_list:
        if not isinstance(adj, str):
            adj_names.append(gdf[name_col][adj])
        elif adj == 'exterior':
            adj_names.append('exterior')
        else:
            adj_idx = int(adj.split(":")[1])
            adj_names.append(f"vertex:{gdf[name_col][adj_idx]}")
    if len(adj_names) > 1 and adj_names[-1].split(":")[-1] == adj_names[0].split(":")[-1]:
        if adj_names[-1].split(":")[0] == 'vertex':
            adj_names.pop()
        else:
            adj_names= adj_names[1:]
    return adj_names

def get_all_indices(region_name, gdf, name_col):
    num_regions = len(gdf.geometry)
    indices = [i for i in range(num_regions) if gdf[name_col][i].split("_")[0] == region_name]
    return indices

def get_idx(region_name, gdf, name_col):
    i = 0
    while gdf[name_col][i] != region_name:
        i += 1
    return i

def get_intersection_info(nbr_edge, rgn_edge, cos_tol = .02):
    # nbr_edge and rgn_edge are LineStrings.
    rgn_edge_len = rgn_edge.length
    nbr_coords = nbr_edge.coords
    v = np.array(nbr_coords[0])
    u = np.array(nbr_coords[1])
    rgn_coords = rgn_edge.coords
    x = np.array(rgn_coords[0])
    y = np.array(rgn_coords[1])
    u_proj = projected_coords(u, x, y)
    v_proj = projected_coords(v, x, y)
    if u_proj > v_proj:
        return None, None
    if abs(v_proj) <= 1e-10:
        return "0d", [0, 0]
    if v_proj <= -1e-10 or u_proj >= rgn_edge_len-1e-10:
        return None, None
    else:
        # Check that (v-u) and (y-x) are almost parallel
        cos = np.dot(v-u, y-x)/(np.linalg.norm(v - u)*np.linalg.norm(y-x))
        if cos >= 1- cos_tol:
            return "1d", [u_proj, v_proj]
        else:
            return "0d", None
    
def plot(gdf, name_col, paper_format = False):
    if paper_format:
        fig = plt.figure()
        ax = fig.add_axes([0, 0, 1, 1])
        ax.axis('off')
        gdf.plot(ax = ax)
    else:
        gdf["center"] = gdf["geometry"].centroid
        gdf.plot(figsize = (25, 20), color = "whitesmoke", edgecolor = "lightgrey", linewidth = 0.5)
        num_regions = len(gdf.geometry)
        for i in range(num_regions):
            region_name = gdf[name_col][i]
            center = gdf["center"][i]
            plt.text(center.x, center.y, region_name, fontsize = 8)
    plt.show()
    
def plot_attribute(gdf, name_col, region_vals, exclude = None):
    fig, ax = plt.subplots()
    ax.set_axis_off()
    attribute = []
    if exclude is not None:
        drop_rows = [i for i, row in gdf.iterrows() if row[name_col] in exclude]
    else:
        drop_rows = []
    new_gdf = gdf.drop(drop_rows)
    for i, row in new_gdf.iterrows():
        parent_rgn = row[name_col].split("_")[0]
        parent_rgn_val = region_vals[parent_rgn]
        attribute.append(parent_rgn_val)
    new_gdf['attribute'] = attribute
    new_gdf.plot(ax = ax, column = 'attribute', legend = True)

def plot_filtration(gdf, name_col, region_values):
    alphas = []
    for i, row in gdf.iterrows():
        if row[name_col] not in region_values:
            parent_rgn = row[name_col].split("_")[0]
            parent_rgn_val = region_values[parent_rgn]
            region_values.update({row[name_col] : parent_rgn_val})
    attribute = [region_values[row[name_col]] for i, row in gdf.iterrows()]
    gdf['attribute'] = attribute
    alphas = sorted(set(region_values.values()))
    for alpha in alphas:
        plot_sublevel(gdf, alpha, 'attribute')

def plot_filtration_subset(gdf, name_col, region_values, title = None):
    # gdf: shape file. 
    # name_col: string. Name of the column that stores region names (e.g. 'NAME' for CA counties)
    # regions_to_plot: List. Names of regions that we want to include in complex.
    # region_values: Dictionary whose keys are ALL region names (superset of regions_to_plot), 
    # value is case count on some day.
    alphas = []
    for i, row in gdf.iterrows():
        if row[name_col] not in region_values:
            parent_rgn = row[name_col].split("_")[0]
            parent_rgn_val = region_values[parent_rgn]
            region_values.update({row[name_col] : parent_rgn_val})
        
    alphas = sorted(set(region_values.values()))
    for alpha in alphas:
        for region in region_values:
            if region_values[region] <= alpha:
                plot_region(region, gdf, name_col)
        plt.title(fr"Filtration value $\leq {alpha}$")
        plt.show()

def plot_legend(gdf, name_col, region_colors):
    plt.figure(figsize = (10, 10))
    for i, row in gdf.iterrows():
        name = row[name_col]
        if name in region_colors:
            color = region_colors[name]
        else:
            color = "whitesmoke"
        plot_region_by_idx(i, gdf, name_col, paper_format = True, color = color)
        
def plot_log_attribute(gdf, name_col, region_vals):
    num_regions = len(gdf.geometry)
    attribute = [math.log(max(region_vals[gdf[name_col][i]], 1)) for i in range(num_regions)]
    gdf['attribute'] = attribute
    gdf.plot(column = 'attribute', legend = True)

def plot_polygon(adj, fname = None, fontsize = 30, int_rgn = False):
    plt.figure(figsize = (8, 8))
    plt.axis('off')
    num_edges = len([nbr for nbr in adj if nbr.split(":")[0] != 'vertex'])
    poly_xs = []
    poly_ys = []
    i = 0
    for nbr in adj:
        if nbr.split(":")[0] == 'vertex':
            theta = -2*math.pi*i/num_edges
            x = math.cos(theta)
            y = math.sin(theta)
            if theta >= -math.pi:
                va = 'top'
            else:
                va = 'bottom'
            if -3*math.pi/2 <= theta and theta <= -math.pi/2:
                ha = 'right'
            else:
                ha = 'left'    
            plt.plot(x, y, 'o', markersize = 12, color = 'k')
            plt.text(x, y, 'v', fontsize = fontsize, horizontalalignment = ha, verticalalignment = va)
        else:
            theta1 = -2*math.pi*i/num_edges
            theta2 = -2*math.pi*(i+1)/num_edges
            xs = [math.cos(theta1), math.cos(theta2)]
            ys = [math.sin(theta1), math.sin(theta2)]
            poly_xs.append(xs[0])
            poly_ys.append(ys[0])
            ha = 'center'
            theta = (theta1 + theta2)/2
            x = (xs[0] + xs[1])/2
            y = (ys[0] + ys[1])/2
            if theta >= -math.pi:
                va = 'top'
            else:
                va = 'bottom'
            if xs[0] == xs[1]:
                if ys[0] < ys[1]:
                    angle = 90
                else:
                    angle = -90
            else:
                slope = (ys[1] - ys[0])/(xs[1] - xs[0])
                angle = math.degrees(math.atan(slope))
            plt.text(x, y, nbr, fontsize = fontsize, rotation = angle, horizontalalignment = ha, verticalalignment = va, rotation_mode = 'anchor')
            i += 1
        plt.fill(poly_xs, poly_ys, facecolor = 'tab:blue', edgecolor = 'k')
    if int_rgn:
        thetas = [2*math.pi*k/3 for k in range(3)]
        xs = [math.cos(theta) for theta in thetas]
        ys = [math.sin(theta) for theat in thetas]
        #plt.fill(xs, ys, facecolor = )
    if fname is not None:
        plt.savefig(fname, bbox_inches='tight')
    plt.show()
        
def plot_region(name, gdf, name_col, paper_format = False):
    i = get_idx(name, gdf, name_col)
    plot_region_by_idx(i, gdf, name_col, paper_format)
    
def plot_region_by_idx(i, gdf, name_col, paper_format = False, color = None, with_label = True):
    rgn_name = gdf[name_col][i]
    rgn_name = rgn_name.split("_")[0]
    if paper_format:
        plt.axis('off')
    if len(rgn_name.split(" ")) == 1:
        rgn_name = '-'.join([word.capitalize() for word in rgn_name.split("-")])
    else:
        rgn_name = ' '.join([word.capitalize() for word in rgn_name.split(" ")])
    if str(gdf['geometry'].geom_type[i]) == 'MultiPolygon':
        multi_poly = gdf['geometry'][i]
        for poly in multi_poly:
            coords = poly.exterior.coords
            xs = [p[0] for p in coords]
            ys = [p[1] for p in coords]
            if paper_format:
                if color is None:
                    if with_label:
                        p = plt.fill(xs, ys, label = rgn_name, edgecolor = 'k')
                    else:
                        p = plt.fill(xs, ys, edgecolor = 'k')
                    color = p[0].get_facecolor()
                else:
                    p = plt.fill(xs, ys, facecolor = color, edgecolor = 'k')

            else:
                plt.plot(xs, ys, 'o')
                center= poly.centroid
                if with_label: plt.text(center.x, center.y, rgn_name, fontsize = 8)
                plt.plot(xs, ys, markersize = 1)
    else:
        gdf["center"] = gdf["geometry"].centroid
        coords = gdf['geometry'][i].exterior.coords
        xs = [p[0] for p in coords]
        ys = [p[1] for p in coords]
        if paper_format:
            if color is not None:
                if with_label:
                    p = plt.fill(xs, ys, label = rgn_name, edgecolor = 'k', color = color)
                else:
                    p = plt.fill(xs, ys, edgecolor = 'k', color = color)
            else:
                if with_label:
                    p = plt.fill(xs, ys, label = rgn_name, edgecolor = 'k')
                else:
                    p = plt.fill(xs, ys,  edgecolor = 'k')
        else :
            plt.plot(xs, ys, 'o')
            plt.plot(xs, ys, markersize = 1)
            center = gdf["center"][i]
            if with_label: plt.text(center.x, center.y, gdf[name_col][i], fontsize = 8)
    if paper_format: return p

def plot_region_with_nbrs(region_name, gdf, name_col, paper_format = False, fname = None):
    if paper_format:
        color = None
        plt.axis('off')
        region_name = region_name.split("_")[0]
        region_indices = get_all_indices(region_name, gdf, name_col)
        nbrs = []
        for i in region_indices:
            if color is None:
                p = plot_region_by_idx(i, gdf, name_col, paper_format)
                color = p[0].get_facecolor()
            else:
                plot_region_by_idx(i, gdf, name_col, paper_format, color = color, with_label = False)
            nbrs += gdf['Neighbors'][i]
        nbrs = set(nbrs)
    else:
        plt.figure(figsize = (20, 20))
        plt.title(region_name)
        i = get_idx(region_name, gdf, name_col)
        plot_region_by_idx(i, gdf, name_col, paper_format)
        nbrs = gdf['Neighbors'][i]
    if not 'Neighbors' in gdf.columns:
        gdf = find_neighbors(gdf)
    for n in nbrs:
        plot_region_by_idx(n, gdf, name_col, paper_format)
    if paper_format:
        plt.legend(prop={'size': 15}, loc = "center right", bbox_to_anchor = (0, .5))
    if fname is not None:
        plt.savefig(fname, bbox_inches='tight')
    plt.show()
    
def plot_sublevel(gdf, thresh, attribute):
    sublevel = [1 if row[attribute] <= thresh else 0 for i, row in gdf.iterrows()]
    gdf['sublevel'] = sublevel
    plt.title(f"Threshold = {thresh}")
    gdf.plot(column = 'sublevel', legend = True)

def plot_with_highlighted_region(highlight_region, gdf, name_col):
    gdf["center"] = gdf["geometry"].centroid
    gdf.plot(figsize = (25, 20), color = "whitesmoke", edgecolor = "lightgrey", linewidth = 0.5)
    num_regions = len(gdf.geometry)
    for i in range(num_regions):
        region_name = gdf[name_col][i]
        center = gdf["center"][i]
        if region_name == highlight_region:
            plt.text(center.x, center.y, region_name, fontsize = 8, color = 'r')
        else:
            plt.text(center.x, center.y, region_name, fontsize = 8)
    plt.show()
 
def projected_coords(u, x, y):
    # u, x, and y are np.arrays of length 2
    v = y-x
    proj_coord = np.dot((u-x), v)/np.linalg.norm(v)
    return proj_coord

def read_adj(filename):
    # filename encodes the adjacencies of the regions. First word of each
    # line is the region whose adjacencies we're reading. Then we list
    # the adjacent regions, in clockwise order. Regions are separated by tabs. 
    adj_file = open(filename, "r")
    adj = {}
    lines = adj_file.read().splitlines()
    for L in lines:
        L = L.split("\t")   # First word in L is the name of the nbhd. Then a tab. Then adjacent nhbds are separated by tabs.
        nbhd = L[0]
        nbrs = [nbr for nbr in L[1:] if len(nbr) > 0]  # line may have extra tab at end, so get rid of empty spaces
        adj.update({nbhd : nbrs})
    return adj
        
def read_shp(shp_file, name_col):
    gdf = gpd.read_file(shp_file)
    gdf = separate_components(gdf, name_col)
    gdf = find_neighbors(gdf)
    return gdf

def remove_islands(gdf):
    # For every Multipolygon, remove all but the largest (by area) Polygon
    # (It's not *necessarily* removing islands, but that is what it's doing for 
    # CA counties, which is the only shapefile it's called for.)
    # Result is a shapefile where every element has geom_type Polygon.
    no_multi = gdf
    num_nbhds = len(gdf['geometry'])
    new_geom = [gdf['geometry'][i] for i in range(num_nbhds)]
    for i in range(num_nbhds):
        if str(gdf['geometry'].geom_type[i]) == 'MultiPolygon':
            multi_poly = gdf['geometry'][i]
            num_polys = len(multi_poly)
            max_area = 0
            for j in range(num_polys):
                poly = multi_poly[j]
                if poly.area > max_area: 
                    max_area = poly.area
                    max_index = j
            new_geom[i] = multi_poly[max_index]
    no_multi['newgeom'] = new_geom
    no_multi = no_multi.set_geometry('newgeom')
    no_multi = no_multi.rename(columns = {'geometry' : 'oldgeom'})
    no_multi = no_multi.rename(columns = {'newgeom' : 'geometry'}).set_geometry('geometry')
    return no_multi

def save_adj(adj, filename):
    with open(filename, "w") as f:
        for rgn in adj:
            f.write(rgn + "\t")
            for nbr in adj[rgn]:
                f.write(nbr + "\t")
            f.write("\n")

def save_neighbors(gdf, shpfile):
    assert 'Neighbors' in gdf.columns, "Need to compute neighbors before you can save them"
    for i, rgn in gdf.iterrows():
        nbrs_as_text = ""
        for nbr in rgn['Neighbors']:
            nbrs_as_text += f"{nbr}\t"
        gdf['Neighbors'][i] = nbrs_as_text
    gdf.to_file(shpfile)

def separate_components(gdf, name_col):
    # Rename entries that show up more than once
    region_comp_count = {row[name_col] : 0 for i, row in gdf.iterrows()}
    region_first_index = {}
    for i, row in gdf.iterrows():
        rgn_name = row[name_col]
        if region_comp_count[rgn_name] == 0:
            region_first_index.update({rgn_name : i})
        else:
            if region_comp_count[rgn_name] == 1:
                gdf[name_col][region_first_index[rgn_name]] += '_0' # If this is the first duplicate of the region, go back and add a counter to the original instance of the region
            gdf[name_col][i] += f'_{region_comp_count[rgn_name]}'
        region_comp_count[rgn_name] += 1
    
    # Separate multipolygons
    new_entries = None
    entries_to_drop = []
    for i, row in gdf.iterrows():
        if str(gdf['geometry'].geom_type[i]) == 'MultiPolygon':
            entries_to_drop.append(i)
            rgn_name = row[name_col]
            multi_poly = gdf['geometry'][i]
            for j, poly in enumerate(multi_poly):
                new_entry = gdf.iloc[[i]]
                new_entry['geometry'] = poly
                new_entry[name_col] = rgn_name + f"_{j}"
                if new_entries is None:
                    new_entries = new_entry
                else:
                    new_entries = pd.concat([new_entries, new_entry], ignore_index=True)
    gdf.drop(entries_to_drop, inplace = True)
    gdf = gpd.GeoDataFrame(pd.concat([gdf, new_entries], ignore_index=True), crs = gdf.crs)
    return gdf
    
def update_uvlist(uvlist, new_uv, nbr, dist):
    # new_uv is a list with 2 elements
    i = 0
    while i < len(uvlist):
        uv = uvlist[i]
        is_uv_subset = max(0, uv[0]) >= max(0, new_uv[0]) and uv[1] <= new_uv[1] and uv[1] - uv[0] > 1e-10
        is_new_uv_subset = max(0, new_uv[0]) >= max(0, uv[0]) and new_uv[1] <= uv[1] and new_uv[1] - new_uv[0] > 1e-10
        if is_uv_subset or is_new_uv_subset:
            if nbr == uv[2]:
                if is_uv_subset:
                    uvlist.remove(uv)
                else:
                    return
            else:
                new_dist = dist
                other_dist = uv[3]
                if abs(new_dist-other_dist)<1e-10 and is_uv_subset:
                    print("subsets of each other and distances are the same = ", new_dist)
                    print("nbrs= ", nbr, ", ", uv[2])
                    print(f"{nbr} intersection = {new_uv}", )
                    print(f"{uv[2]} intersection = {uv[0], uv[1]}")
                    print("removing ", uv[2])
                    uvlist.remove(uv)
                elif new_dist < other_dist:
                    uvlist.remove(uv)
                    print("removing ", uv[2])
                else:
                    return
        else:
            i += 1
    eps = 5e-5
    if len(uvlist) > 0 and nbr == uvlist[-1][2] and abs(new_uv[0] - uvlist[-1][1]) < eps:
        uvlist[-1] = [uvlist[-1][0], new_uv[1], nbr, min(dist, uvlist[-1][3])]
    else:
        new_uv.append(nbr)
        new_uv.append(dist)
        uvlist.append(new_uv)
        
if __name__ == "__main__":
    
    '''
    NYC modified ZTCA
    Original shapefile source: https://data.cityofnewyork.us/Health/Modified-Zip-Code-Tabulation-Areas-MODZCTA-/pri4-ifjk/data
    Already separated multipolygons and computed neighbors.
    '''
    nyc_shp = 'data/NYC_modzcta/geo_export_90184b1b-c30a-4cf2-a054-e1cf140844fc.shp'
    nyc_gdf = gpd.read_file(nyc_shp)
    name_col = 'modzcta'
    nyc_gdf = find_neighbors(nyc_gdf)
    plot(nyc_gdf, name_col)
    
    '''
    NYC nbhds
    Shapefile source: https://data.cityofnewyork.us/Business/Zip-Code-Boundaries/i8iw-xf4u
    Vaccination data: https://www1.nyc.gov/site/doh/covid/covid-19-data-vaccines.page
    
    No longer using this shapefile.
    '''
    # nyc_shp = 'data/NYC_zipcodes/ZIP_CODE_040114.shp'
    # nyc_gdf = gpd.read_file(nyc_shp)
    # name_col = 'ZIPCODE'
    # nyc_gdf = find_neighbors(nyc_gdf, shpfile = nyc_shp)
    # separate_components(nyc_gdf, name_col)
    
    '''
    LA Nbhds
    '''
    # Initial set up
    la_shp = 'data/LA-shp/COVID19_by_Neighborhood.shp'
    la_gdf = gpd.read_file(la_shp)
    name_col = 'COMTY_NAME'
    la_gdf[name_col][110] = 'HYDE PARK_1'
    la_gdf[name_col][111] = 'HYDE PARK_2'
    la_gdf[name_col][55] =  'TEMPLE-BEAUDRY_2'
    la_gdf[name_col][56] = 'TEMPLE-BEAUDRY_1'
    la_gdf = find_neighbors(la_gdf, 5e-4, la_shp)
    
    plot_region_with_nbrs('KOREATOWN', la_gdf, name_col, paper_format = True)
    
    # Make figures
    la_gdf['Neighbors'][get_idx('WEST VERNON', la_gdf, name_col)].append(get_idx('VERMONT SQUARE', la_gdf, name_col))   # only necessary for plotting neighbors in paper format
    la_gdf['Neighbors'][get_idx('KOREATOWN', la_gdf, name_col)].remove(get_idx('VICTORIA PARK', la_gdf, name_col))       # only necessary for plotting neighbors in paper format
    plot_region_with_nbrs('MIRACLE MILE', la_gdf, name_col, paper_format = True, fname = 'figures/miraclemile_nbrs.png')
    plot_polygon(['Mission Hills', 'North Hills', 'Northridge', 'Porter Ranch', 'exterior'], 'figures/polygon_granada.png')
    plot_polygon(['Wilshire Center', 'Pico-Union', 'Harvard Heights', 'Country Club Park', 'Hancock Park', 'Wilshire Center', 'Little Bangladesh'], 'figures/polygon_koreatown.png', 20)
    plot_polygon(['vertex:Valley Village', 'Sherman Oaks', 'Van Nuys', 'North Hollywood'], 'figures/polygon_valleyglen.png', 40)
    plot_polygon(['West Vernon', 'West Vernon', 'West Vernon'], 'figures/polygon_vermontsquare.png', 40)
    plot_polygon(['Miracle Mile', 'Miracle Mile', 'Hancock Park'], 'figures/polygon_sycamoresquare.png', 40)
    plot_polygon(['Hancock Park', 'Sycamore Square', 'Sycamore Square', 'Hancock Park', 'Brookside', 'Country Club Park', 'Mid-City', 'exterior', 'South Carthay', 'exterior', 'Carthay', 'Park La Brea', 'Melrose'], 'figures/miraclemile_poygon.png', fontsize = 11)
    
    '''
    California Counties
    '''
    # Intial set up
    ca_shp = 'data/CA_counties/CA_counties_TIGER2016.shp'
    ca_gdf = gpd.read_file(ca_shp)
    name_col = 'NAME'
    ca_gdf = remove_islands(ca_gdf)
    ca_gdf = find_neighbors(ca_gdf, shpfile = ca_shp)