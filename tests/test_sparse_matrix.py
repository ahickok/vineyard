# -*- coding: utf-8 -*-
"""
Testing class sparseMatrix and helper classes Node, linkedList.
"""

from vineyard.vineyard import Node
from vineyard.vineyard import linkedList
from vineyard.vineyard import sparseMatrix

''' Testing Node '''
print("Testing Node class")
n1 = Node(1)
print("Testing node constructor\n")
n1.print()
n2 = Node(3)
n3 = Node(5)
n4 = Node(2)
n5 = Node(3)
n6 = Node(4)

''' Testing linkedList '''
print("\nTesting linkedList class")
testList = linkedList()
testList.head = n1
print("Testing linked list constructor\n")
testList.print()

list1 = linkedList()
list1.append(n1)
list1.append(n2)
list1.append(n3)
print("\nTesting append method. This is list1 after appending 1, 3, and 5:\n")
list1.print()

print("\nTesting __iadd__")
list2 = linkedList()
list2.append(n4)
list2.append(n5)
list2.append(n6)
print("\nlist2:\n")
list2.print()

print("\nlist1 += list2, deleting nodes that come in duplicates\n")
list1 += list2
list1.print()
print("\nlist2 after += operation (shouldn't change):\n")
list2.print()

list1.remove(2) # also checked remove(1) (the head) and remove(5) (the tail)
print("\nTesting remove method. list1 after removing node with value 2:\n")
list1.print()
print("\nlist2 (shouldn't change):\n")
list2.print()

n1 = Node(1)
n2 = Node(2)
n3 = Node(2)
list3 = linkedList()
list3.append(n1)
list3.append(n2)
print("list3: \n")
list3.print()

list4 = linkedList()
list4.append(n3)
print("\nlist4:\n")
list4.print()

print("\nlist4 += list3, deleting nodes that come in duplicates\n")
list4 += list3
list4.print()

print("\nlist3 after += operation (shouldn't change):\n")
list3.print()

list5 = linkedList()
list5.append(Node(1))
list5.append(Node(3))
list5.append(Node(5))
list5.append(Node(6))
list5.append(Node(7))
list5.append(Node(8))
list5.append(Node(9))
print("list5:\n")
list5.print()
list6 = linkedList()
list6.append(Node(2))
list6.append(Node(4))
list6.append(Node(7))
print("\nlist6:\n")
list6.print()
list6 += list5
print("\nlist6 += list5\n")
list6.print()   # should be 1, 2, 3, 4, 5, 6, 8, 9
print("\nlist5 after += operation (shouldn't change\n")
list5.print()
list5.remove(3)
print("\nlist5 after removing node with value 3:\n")
list5.print()
print("\nlist6 after node with value 3 was removed from list5 (shouldn't change):")
list6.print()
list6.remove(8)
print("\nlist6 after removing node with value 8\n")
list6.print()
print("\nlist5 after node with value 8 was removed from list6 (shouldn't change):")
list5.print()

list7 = linkedList()
list7.append(Node(1))
list7.append(Node(2))
list7.append(Node(3))
list8 = linkedList()
list8.append(Node(1))
list8.append(Node(2))
print("\nlist7:\n")
list7.print()
print("\nlist8:\n")
list8.print()
list7 += list8
print("\n list7 += list8:\n")
list7.print()
print("\n list8 after += operation (shouldn't change):\n")
list8.print()

list9 = linkedList()
list9.append(Node(1))
list9.append(Node(2))
list10 = linkedList()
list10.append(Node(1))
list10.append(Node(3))
print("\nlist10 += list9:")
list10 += list9
list10.print()

''' Testing sparseMatrix '''
print("\nTesting sparseMatrix class\n")
print("Testing sparseMatrix constructor")
A = sparseMatrix(7) # creates empty 7x7 sparseMatrix
print("Adding non-zero entries to the sparseMatrix:\n")
A.col_list[3].append(Node(0))
A.col_list[3].append(Node(2))
A.col_list[4].append(Node(1))
A.col_list[4].append(Node(2))
A.col_list[5].append(Node(0))
A.col_list[5].append(Node(1))
A.col_list[6].append(Node(3))
A.col_list[6].append(Node(4))
A.col_list[6].append(Node(5))
A.print()

print("\nTesting low method. Returns -1 if the column is zero vector.")
for i in range(A.n):
    print(f"Lowest non-zero index in col {i} is {A.low(i)}")
    
print("\nTesting reduce method. The reduced matrix:\n")
A.reduce()
A.print()

print("Testing is_positive method. Must be called AFTER matrix is reduced.\n")
for i in range(7):
    print(f"col {i} represents positive simplex (zero col)? {A.is_positive(i)}")

print("\nNew sparseMatrix:")
A = sparseMatrix(4)
A.col_list[0].append(Node(1))
A.col_list[0].append(Node(2))
A.col_list[1].append(Node(2))
A.col_list[3].append(Node(0))
A.print()

print("\nTesting __getitem__ method\n")
print("The (2, 1) entry is: ", A[2, 1])
print("The (0, 0) entry is: ", A[0,0])

print("\nTesting swap_rows method. After swapping rows 1 and 2:")
A.swap_rows(1,2)
A.print()

print("\nTesting low method after swap_rows has been called. Returns -1 if the column is zero vector.")
for i in range(A.n):
    print(f"Lowest non-zero index in col {i} is {A.low(i)}")
    
print("\nTesting set_zero method. Matrix after setting (1,0) to 0:")
A.set_zero(1, 0)
A.print()

print("\nTesting swap_cols method. After swapping cols 0 and 1:")
A.swap_cols(0, 1)
A.print()

print("\nTesting swap_rows method to see if it works afer swap_cols has been called.")
print("After swapping rows 0 and 2:")
A.swap_rows(0, 2)
A.print()
print("\nAfter swapping rows 0 and 1")
A.swap_rows(0, 1)
A.print()

print("\nTesting add_col method. After adding col 0 to col 1:")
A.add_col(0, 1)
A.print()

print("\nTesting identity method:")
sparseMatrix.identity(5).print()